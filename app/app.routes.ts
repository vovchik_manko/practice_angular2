import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { lecturesRoutes } from './components/lectures/lectures.routes';
import {UsersComponent} from "./components/users/users";

const appRoutes: Routes = [
  {
    path: '',
    component: UsersComponent
  },
  ...lecturesRoutes
];

export const appRouting: ModuleWithProviders = RouterModule.forRoot(appRoutes);

import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {ILectureDetails} from "./lecture-details.model";
import LectureDetailsService from "./lecture-details.service";

@Component({
  selector: 'lecture-details',
  //templateUrl: 'app.html',
  template: `
    <template [ngIf]="lectureDetails">
      {{lectureDetails.description}}<br>
      Author: {{lectureDetails.author}}
    </template>
  `
})

export class LectureDetailsComponent implements OnChanges {
  @Input() lectureId: number;

  lectureDetails: ILectureDetails = null;

  constructor(private lectureDetailsService: LectureDetailsService){

  }

  ngOnChanges(changes: SimpleChanges){
    this.lectureDetails = this.lectureDetailsService.fetchLectureDetails(this.lectureId);
  }
}
